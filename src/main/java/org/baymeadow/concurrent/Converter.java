package org.baymeadow.concurrent;

/**
 * 
 * @param <SRC> source type to convert from
 * @param <DST> destination type to convert to
 */
public interface Converter<SRC, DST> {
	
	/**
	 * Method that will take a {@code SRC} object and convert it into a
	 * {@code DST} object.
	 * 
	 * @param src The source object to convert.
	 * 
	 * @return Converted object of {@code DST} type.
	 */
	public DST apply(SRC src);
	
	
	/**
	 * Helper Converter that will convert objects to strings by invoking their
	 * {@link Object#toString()} method.
	 */
	public static final Converter<Object, String> TO_STRING = new Converter<Object, String>() {
		@Override public String apply(Object source) {
			return (source == null) ? "null" : source.toString();
		}
	};
	
	/**
	 * Helper Converter that will convert objects to Void. This is useful when all
	 * you care about is to be notified when something is complete.
	 */
	public static final Converter<Object, Void> TO_VOID = new Converter<Object, Void>() {
		@Override public Void apply(Object source) {
			return null;
		}
	};
	
	/**
	 * Helper Converter that will convert strings to integers by using the
	 * {@link Integer#valueOf(String)} method.
	 */
	public static final Converter<String, Integer> STRING_TO_INTEGER = new Converter<String, Integer>() {
		@Override public Integer apply(String source) {
			return Integer.valueOf(source);
		}
	};
	
	// This would be the lambda way to define the method above, but we are looking to
	// be able to run this on Java 7, so no lambdas. Leaving it here for future reference.
	// public static final Converter<String, Integer> STRING_TO_INTEGER = source -> Integer.valueOf(source);

	public static <T> Converter<T, T> identity() {
		// return t -> t; Java 8 Lambda Code
		return new Converter<T, T>() {
			@Override public T apply(T src) {
				return src;
			}
		};
	}

}
