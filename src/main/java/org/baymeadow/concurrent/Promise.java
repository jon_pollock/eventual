package org.baymeadow.concurrent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.FutureTask;

/**
 * A {@link Promise} object is used for deferred and asynchronous computations,
 * where the value is expected in the future. This implementation is modeled on
 * the standard JavaScript Promise object.
 * 
 * <p>A major difference between JavaScript and Java is the Threading model. Java
 * supports multiple threads and real concurrency, whereas JavaScript is single
 * threaded. To that end, the API has be augmented to include a mechanism to 
 * specify where a given task should run, an {@link Executor}.
 * 
 * @author Jonathan Pollock
 *
 * @param <V> The result type returned by this class's {@link #get()} method
 */
public class Promise<V> extends FutureTask<V>{
   protected Receiver head_receiver = null;
   protected Receiver tail_receiver = null;

   protected boolean finished = false;
   protected final StackTrace trace;
   protected Promise<?> parent = null;
   protected List<Promise<V>> parents = null;

   private static class StackTrace extends Exception {
      private static final long serialVersionUID = 1L;
      private StackTraceElement[] fixedTrace = null;

      public StackTrace() {
         super("Created on Thread: " + Thread.currentThread().getName());
      }

      public void fixStackTrace() {
         StackTraceElement[] stack = super.getStackTrace();
         int len = stack.length;
         int i = 0;
         while (i<len && stack[i].getClassName().equals(Promise.class.getName())) { i++; };
         fixedTrace = Arrays.copyOfRange(stack, i, len);
         setStackTrace(fixedTrace);
      }
   }


   public Promise() {
      super(Threads.NOOP, null);
      this.trace = new StackTrace();
   }
   public Promise(Callable<V> callable) {
      super(callable);
      this.trace = new StackTrace();
   }
   public Promise(Runnable runable, V value) {
      super(runable, value);
      this.trace = new StackTrace();
   }

   private Promise(Promise<?> parent) {
      super(Threads.NOOP, null);
      this.trace = new StackTrace();
      this.parent = parent;
   }

   private Promise(List<Promise<V>> parents) {
      super(Threads.NOOP, null);
      this.trace = new StackTrace();
      this.parents = new ArrayList<Promise<V>>(parents);
   }

   static public interface Failed {
      public void failed(Throwable reason);
   }

   static public interface Rejected<V> {
      public V rejected(Throwable t) throws Throwable;
   }

   /**
    * We are overriding this method to add a trace which indicates where the
    * promise was created.
    */
   @Override protected void setException(Throwable err) {
      Throwable t = err;
      Throwable cause = err.getCause();
      while (cause != null && !(cause instanceof StackTrace)) {
         t = cause;
         cause = t.getCause();
      }

      if (cause == null) {
         // try to add the location where we created the promise.
         trace.fixStackTrace(); // remove the promise methods
         try { t.initCause(trace); } catch (IllegalStateException e) { }
      }
      super.setException(err);
   }

   protected abstract class Receiver implements Runnable {
      protected final Executor executor;
      public Receiver next;

      public Receiver(Executor executor) {
         this.executor = executor;
         this.next = null;
      }

      public void perform() {
         if (executor == null) {
            run();
         } else {
            executor.execute(this);
         }
      }
   }

   /**
    * This method will unwrap an {@link Throwable} {@link ExecutionException} into
    * it's underlying cause. This is needed because the {@link Promise#get()} method
    * will wrap the underlying exception.
    * 
    * @param t exception that might need to be unwrapped.
    * @return unwrapped throwable cause for the execution exception.
    */
   private static Throwable unwrapException(Throwable t) {
      while (t instanceof ExecutionException) {
         if (t.getCause() == null) break;
         t = t.getCause();
      }
      return t;
   }


   protected void addReceiver(Receiver receiver) {
      boolean performNow = false;
      synchronized (this) {
         if (finished) {
            performNow = true;
         } else {
            if (tail_receiver == null) {
               head_receiver = tail_receiver = receiver;
            } else {
               tail_receiver.next = receiver;
               tail_receiver = receiver;
            }
         }
      }
      if (performNow) {
         receiver.perform();
      }
   }

   /**
    * Protected method invoked when this task transitions to state
    * {@code isDone} (whether normally or via cancellation). In this case,
    * The promise receivers will need to be invoked. This will happen on the
    * same thread that the promise's {@link #set(Object)}, {@link #setException(Throwable)},
    * {@link #resolve(Object)}, {@link #reject(Throwable)} or {@link #cancel(boolean)} method was invoked on.
    */
   @Override protected void done() {
      Receiver receiver = null;
      
      while (true) {
         synchronized (this) {
            receiver = head_receiver;
            if (receiver != null) {
               head_receiver = receiver.next;
            } else {
               finished = true;
               tail_receiver = null;
               break;
            }
         }
         
         receiver.perform(); 
      }
   }

   /**
    * This method performs a {@link #set(Object)} with the value. This basically
    * make the promise object look more like the JavaScript version of Promises.
    * 
    * @param value value that the promise should return
    */
   public void resolve(V value) { set(value); }

   /**
    * This method performs a {@link #setException(Throwable)} with the exception
    * that the promise should return. The method exists primarily to make this
    * object look more like the JavaScript version of Promises.
    * 
    * @param reason exception that the promise should return
    */
   public void reject(Throwable reason) { setException(reason); }

   public static interface Composer<SRC, DST> {
      public Promise<DST> apply(SRC src);
   }

   protected void applyTo(Promise<V> other) {
      addReceiver(new Receiver(null) {
         @Override public void run() {
            V value;
            try {
               value = Promise.this.get();
               other.set(value);
            } catch (CancellationException canceled) {
               other.cancel(true);
            } catch (InterruptedException | ExecutionException e) {
               Throwable t = unwrapException(e);
               other.setException(t);
            }
         }
      });
   }

   public <T> Promise<T> then(Executor executor, Composer<V, T> composer) {
      final Promise<T> promise = new Promise<T>(this);
      addReceiver(new Receiver(executor) {
         @Override public void run() {
            V value;
            Promise<T> newPromise;
            try {
               value = Promise.this.get();
               newPromise = composer.apply(value);
               newPromise.applyTo(promise);
            } catch (CancellationException canceled) {
               promise.cancel(true);
            } catch (InterruptedException | ExecutionException e) {
               Throwable t = unwrapException(e);
               promise.setException(t);
            }
         }
      });
      return promise;
   }

   public <T> Promise<T> then(Converter<V, T> onResolvedConvert) {
      return then(null, onResolvedConvert, null);
   }

   public <T> Promise<T> then(Converter<V, T> onResolvedConvert, Rejected<T> onRejected) {
      return then(null, onResolvedConvert, onRejected);
   }

   public <T> Promise<T> then(Executor executor, Converter<V, T> onResolvedConvert) {
      return then(executor, onResolvedConvert, null);
   }

   public <T> Promise<T> then(Executor executor, final Converter<V, T> onResolvedConvert, final Rejected<T> onRejected) {
      final Promise<T> promise = new Promise<T>(this);

      addReceiver(new Receiver(executor) {
         @Override public void run() {
            V value;
            T newValue;
            try {
               value = Promise.this.get();
               try {
                  newValue = onResolvedConvert.apply(value);
               } catch (Throwable t) {
                  promise.reject(t);
                  return;
               }
            } catch (CancellationException canceled) {
               promise.cancel(true);
               return;
            } catch (InterruptedException | ExecutionException e) {
               Throwable t = unwrapException(e);
               if (onRejected != null) {
                  try {
                     newValue = onRejected.rejected(t);
                  } catch (Throwable rejectedError) {
                     promise.reject(rejectedError);
                     return;
                  }
               } else {
                  promise.reject(t);
                  return;
               }
            }

            try {
               promise.resolve(newValue);
            } catch (Throwable e) {
               promise.reject(e);
            }
         }

      });

      return promise;
   }

   public Promise<V> error(Executor executor, final Rejected<V> onRejected) {
      final Promise<V> promise = new Promise<V>(this);

      Receiver r = new Receiver(executor) {
         @Override public void run() {
            V value;
            try {
               value = Promise.this.get();
            } catch (CancellationException canceled) {
               promise.cancel(true);
               return;
            } catch (InterruptedException | ExecutionException e) {
               if (onRejected != null) {
                  try {
                     value = onRejected.rejected(unwrapException(e));
                  } catch (Throwable rejectedError) {
                     promise.reject(rejectedError);
                     return;
                  }
               } else {
                  promise.reject(unwrapException(e));
                  return;
               }
            }

            try {
               promise.resolve(value);
            } catch (Throwable e) {
               promise.reject(e);
            }
         }
      };
      addReceiver(r);
      return promise;
   }

   public Promise<V> error(Rejected<V> onRejected) {
      return error(null, onRejected);
   }



   /**
    * Creates and returns an already resolved {@link Promise} with the supplied
    * value.
    * 
    * @param value supplied value that the promise will return.
    * 
    * @return newly allocated and completed promise with the supplied value.
    */
   public static <T> Promise<T> resolved(T value) {
      Promise<T> result = new Promise<T>();
      result.resolve(value);
      return result;
   }


   public static <T> Promise<T> race(final List<Promise<T>> promises) {
      return race(promises, true);
   }
   
   public static <T> Promise<T> race(final List<Promise<T>> promises, boolean cancelRunningOnDone) {
      Promise<T> result = new Promise<T>(promises);

      Converter<T, T> success = new Converter<T, T>() {
         @Override public T apply(T value) {
            result.set(value);
            return value;
         }
      };

      Rejected<T> rejected = new Rejected<T>() {
         @Override public T rejected(Throwable t) throws Throwable {
            result.setException(t);
            throw t;
         }
      };

      for (Promise<T> promise : promises) {
         promise.then(success, rejected);
      }
      
      if (cancelRunningOnDone) {
         result.then(new Converter<T, T>() {
            @Override public T apply(T value) {
               for (Promise<T> promise : promises) {
                  if (!promise.isDone()) {
                     promise.cancel(true);
                  }
               }
               return null;
            }
         });
      }

      return result;
   }

}
