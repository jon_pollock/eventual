package org.baymeadow.concurrent;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;

/**
 * This is a utility class to provide some basic thread features. The first useful
 * item is a runnable that does nothing and is a no op, {@link #NOOP}.
 * 
 * @author Jonathan Pollock
 *
 */
public class Threads {
   /**
    * This is a No-Operation Runnable that can be used where there really isn't anything
    * that needs to be done, but a runnable is still required.
    */
   public static final Runnable NOOP = new Runnable() {
      @Override public void run() { }
   };

   private static final ThreadGroup TIMEOUT_THREAD_GROUP = new ThreadGroup("THREADS_TIMEOUT");
   static {
      TIMEOUT_THREAD_GROUP.setDaemon(true);
   }

   /**
    * This is a single threaded timeout scheduler that {@link Eventual#timeout(long, java.util.concurrent.TimeUnit)}
    * uses to register timeouts. This can be used for other purposes as well, but you really should not
    * perform any long running tasks on this thread as that could interfere with other timeouts.
    */
   public static final ScheduledExecutorService TIMEOUT_SCHEDULER = Executors.newScheduledThreadPool(1, new ThreadFactory() {
      private int threadTimeoutNumber = 0;
      private synchronized int nextTimeoutThreadNum() { return ++threadTimeoutNumber; }
      @Override public Thread newThread(Runnable r) {
         return new Thread(TIMEOUT_THREAD_GROUP, r, TIMEOUT_THREAD_GROUP.getName() + "-" + nextTimeoutThreadNum());
      }
   });
}
