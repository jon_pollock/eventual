package org.baymeadow.concurrent;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

import org.baymeadow.concurrent.Converter;
import org.baymeadow.concurrent.Promise;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class PromiseTest {
   protected static ExecutorService executor1 = null;
   protected static ExecutorService executor2 = null;
   protected static ExecutorService executor3 = null;

   @BeforeClass public static void setUpBeforeClass() throws Exception {
      executor1 = Executors.newSingleThreadExecutor(new ThreadFactory() {
         @Override public Thread newThread(Runnable r) {
            Thread t = new Thread(r, "EXECUTOR1");
            t.setDaemon(true);
            return t;
         }
      });
      executor2 = Executors.newSingleThreadExecutor(new ThreadFactory() {
         @Override public Thread newThread(Runnable r) {
            Thread t = new Thread(r, "EXECUTOR2");
            t.setDaemon(true);
            return t;
         }
      });
      executor3 = Executors.newSingleThreadExecutor(new ThreadFactory() {
         @Override public Thread newThread(Runnable r) {
            Thread t = new Thread(r, "EXECUTOR3");
            t.setDaemon(true);
            return t;
         }
      });
   }


   @AfterClass public static void tearDownAfterClass() throws Exception {
      List<Runnable> tasks = executor1.shutdownNow();
      if (!tasks.isEmpty()) {
         System.out.println("Executor1 still had " + tasks.size() + " tasks running");
      }
      executor1 = null;
      tasks = executor2.shutdownNow();
      if (!tasks.isEmpty()) {
         System.out.println("Executor2 still had " + tasks.size() + " tasks running");
      }
      executor2 = null;
      tasks = executor3.shutdownNow();
      executor3 = null;
      if (!tasks.isEmpty()) {
         System.out.println("Executor3 still had " + tasks.size() + " tasks running");
      }
   }



   @Test
   public void testPromiseCallableOfString() throws InterruptedException, ExecutionException {
      Promise<String> p1 = new Promise<String>(() -> "123");
      p1.run();
      assertEquals("123", p1.get());
   }

   @Test
   public void testPromiseCallableOfStringIntegerRun() throws InterruptedException, ExecutionException {
      Promise<String> p1 = new Promise<String>(() -> "123");
      Promise<Integer> p2 = p1.then((value) -> Integer.valueOf(value));
      p1.run();
      assertEquals("123", p1.get());
      assertEquals(123, p2.get().intValue());
   }

   @Test
   public void testPromiseCallableOfStringIntegerStringIntegerRun() throws InterruptedException, ExecutionException {
      Promise<String> p1 = new Promise<String>(() -> "123");
      Promise<Integer> p2 = p1.then(new Converter<String, Integer>() {
         @Override public Integer apply(String value) {
            return Integer.valueOf(value);
         }
      });
      Promise<String> p3 = p1.then((value) -> value + value);
      Promise<Integer> p4 = p2.then((value) -> value + value);
      p1.run();
      assertEquals("123", p1.get());
      assertEquals(123, p2.get().intValue());
      assertEquals("123123", p3.get());
      assertEquals(246, p4.get().intValue());
   }

   @Test
   public void testPromiseCallableOfStringRunInteger() throws InterruptedException, ExecutionException {
      Promise<String> p1 = new Promise<String>(() -> "123");
      p1.run();
      Promise<Integer> p2 = p1.then((value) -> Integer.valueOf(value));
      assertEquals("123", p1.get());
      assertEquals(123, p2.get().intValue());
   }


   @Test
   public void testPromiseCallableOfStringIntegerRunException() throws InterruptedException, ExecutionException {
      Promise<String> p1 = new Promise<String>(() -> "abc");
      Promise<Integer> p2 = p1.then((value) -> Integer.valueOf(value));
      executor1.submit(p1);
      assertEquals("abc", p1.get());
      try {
         p2.get();
         fail("Should have thrown an ExecutionException");
      } catch (ExecutionException e) {
         assertEquals(NumberFormatException.class, e.getCause().getClass());
      }
   }

   @Test
   public void testPromiseCallableOfStringIntegerIntegerRejectedRunException() throws InterruptedException, ExecutionException {
      Promise<String> p1 = new Promise<String>(() -> "abc");
      Promise<Integer> p2 = p1.then((value) -> Integer.valueOf(value));
      Promise<Integer> p3 = p2.then(value -> value * 2, (error) -> Integer.valueOf(-1));
      executor1.submit(p1);
      assertEquals("abc", p1.get());
      try {
         p2.get();
         fail("Should have thrown an ExecutionException");
      } catch (ExecutionException e) {
         assertEquals(NumberFormatException.class, e.getCause().getClass());
      }
      assertEquals(-1, p3.get().intValue());
   }

   /**
    * The point of this test is to see whether the exception generated by the converter
    * parsing "abc" into an integer in the <code>p2</code> promise is passed into
    * the <code>p3</code> promise.
    * 
    * @throws InterruptedException
    * @throws ExecutionException
    */
   @Test
   public void testPromiseCallableOfStringIntegerIntegerRunException() throws InterruptedException, ExecutionException {
      Promise<String> p1 = new Promise<String>(() -> "abc");
      Promise<Integer> p2 = p1.then((value) -> Integer.valueOf(value));
      Promise<Integer> p3 = p2.then(value -> value * 2);
      executor1.submit(p1);
      assertEquals("abc", p1.get());
      try {
         p2.get();
         fail("p2.get() Should have thrown an ExecutionException");
      } catch (ExecutionException e) {
         assertEquals(NumberFormatException.class, e.getCause().getClass());
      }

      try {
         p3.get();
         fail("p3.get() Should have thrown an ExecutionException");
      } catch (ExecutionException e) {
         assertEquals(NumberFormatException.class, e.getCause().getClass());
      }
   }

   /**
    * This test case targets the {@link Promise#race(List)} method. It sets up
    * a list of 3 promises where:
    * <ul>
    *  <li>promise 1 will take 1 second and would normally return "p1".
    *  <li>promise 2 will return "p2"
    *  <li>promise 3 will throw a number format exception
    * </ul>
    * 
    * <p>This should cause race to return a result of "p2". The first promise
    * should throw a CancellationException. and the third promise should throw
    * the number format exception.
    * 
    * <p>There is another promise, <code>p4</code>, that which is dependent on
    * promise 1. Since promise 1 is cancelled, this one should also be cancelled.
    * 
    * @throws InterruptedException
    * @throws ExecutionException
    */
   @Test
   public void testRace() throws InterruptedException, ExecutionException {
      Set<String> states = new HashSet<String>();

      List<Promise<String>> promises = new ArrayList<Promise<String>>();
      promises.add(new Promise<String>(new Callable<String>() {
         @Override public String call() throws Exception {
            try {
               synchronized (this) { this.wait(1000); }
               states.add("Should have thrown an InterruptedException");
            } catch (Throwable t) {
               states.add("p0 threw " + t.getClass().getSimpleName());
               throw t;
            }
            return "p1";
         }
      }));

      promises.add(new Promise<String>(() -> "p2"));
      promises.add(new Promise<String>(() -> Integer.valueOf("p3").toString()));

      Promise<String> p4 = promises.get(0).then(value -> value.toUpperCase());

      Promise<String> result = Promise.race(promises);

      executor1.submit(promises.get(0));
      executor2.submit(promises.get(1));
      executor2.submit(promises.get(2));

      assertEquals("p2", result.get());

      try {
         promises.get(0).get();
         fail("Should have thrown a CancellationException.");
      } catch (Throwable ce) {
         states.add("p0.get() threw " + ce.getClass().getSimpleName());
      }


      try {
         p4.get();
         fail("p4 Should have thrown a CancellationException.");
      } catch (Throwable ce) {
         states.add("p4.get() threw " + ce.getClass().getSimpleName());
      }

      assertTrue("p0 should have been interrupted: states=" + states, states.remove("p0 threw InterruptedException"));
      assertTrue("p0.get() should have thrown CancellationException: states=" + states, states.remove("p0.get() threw CancellationException"));
      assertTrue("p4.get() should have thrown CancellationException: states=" + states, states.remove("p4.get() threw CancellationException"));

      assertTrue("states should now be empty, but isn't: states=" + states, states.isEmpty());
   }

}
