Eventual
========

Eventual is a java library that provides a simple mechanism that allows the
programmer to have tasks performed when other tasks finish.

Examples
========
The following example demonstrates a method returning an Eventual object:

```java

/**
 * A method that performs some caclulation, but does it one a separate
 * executor and returns a Eventual<String> object that will be resolved
 * when the task is completed.
 */
public Eventual<String> someLongRunningAsyncMethod() {
    Eventual<String> result = new Eventual<String>(() -> {
        synchronized (this) { this.wait(1000); } // wait one second to simulate
        return "123";
    });
    executor.submit(result);
    return result; 
}
```

And this is how that method might be used:

```java

Eventual<String> result = someLongRunningAsycMethod();
result.timeout(2000, TimeUnit.SECONDS);
result.done((value) -> {
    System.out.println("Result is done with a value of: " + value);
}).fail((t) -> {
    System.out.prinlnt("Failed with the following reason: " + t.getMessage());
});

Eventual<Integer> resultInt = result.map(STRING_TO_INTEGER);
resultInt.done((value) -> {
    System.out.println("Integer result is done with a value of: " + value);
});
```